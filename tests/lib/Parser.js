/**
 * @fileoverview  Formatted text parser base class tests
 * This file is a part of TeXnous project.
 *
 * @copyright TeXnous project team (http://texnous.org) 2017
 * @license LGPL-3.0
 *
 * This unit test is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This unit test is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this unit
 * test; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
 * MA 02111-1307, USA.
 */

"use strict";

const Parser =  require("../../sources/lib/Parser"); // formatted text parser base class

let state, context;



/**
 * Tests of formatted text parser base class
 * @author Kirill Chuvilin <kirill.chuvilin@gmail.com>
 */
module.exports = {
	"State": {
		"constructor": test => {
			test.doesNotThrow(() => { state = new Parser.State(); });
			test.doesNotThrow(() => { new Parser.State(state); });
			test.done();
		},
		"copy": test => {
			let stateCopy;
			test.doesNotThrow(() => { stateCopy = state.copy(); });
			test.equal(state.copy(stateCopy), stateCopy);
			test.done();
		}
	},
	"Context": {
		"constructor": test => {
			test.doesNotThrow(() => { context = new Parser.Context("test source"); });
			test.doesNotThrow(() => { new Parser.Context(context); });
			test.done();
		},
		"copy": test => {
			let contextCopy;
			test.doesNotThrow(() => { contextCopy = context.copy(); });
			test.equal(context.copy(contextCopy), contextCopy);
			test.done();
		},
		"source": test => {
			test.equal(context.source, "test source");
			test.equal(context.sourceOffset, 0);
			test.equal(context.sourceCuttingOffset, 0);
			test.equal(context.sourceCharacter, "t");
			test.equal(context.isAtSourceEnd(), false);
			test.equal(context.sourceProbe("test"), true);
			test.equal(context.sourceProbe("source"), false);
			test.equal(context.sourceProbe(/^source/), false);
			test.equal(context.sourceProbe(/.*source/), true);
			let matchResult = ["source"];
			matchResult.index = 5;
			matchResult.input = context.source;
			test.deepEqual(context.sourceMatch(/source/),	matchResult);
			test.equal(context.sourceProbe(/^test/, true), true);
			test.equal(context.sourceProbe(/^\ssource/), true);
			test.equal(context.sourceOffset, 4);
			test.equal(context.sourceCuttingOffset, 0);
			test.doesNotThrow(() => context.markSourceCuttingOffset());
			test.equal(context.sourceCuttingOffset, 4);
			test.done();
		}
	},
	"Parser": {
		"constructor": test => {
			test.doesNotThrow(() => { new Parser(); });
			test.done();
		}
	}
};
